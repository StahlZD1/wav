//
//  LogInController.swift
//  wav
//
//  Created by David Goehring on 2/18/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import UIKit

var locationManager : CLLocationManager!

class LogInController: UIViewController, SignUpDelegate {
    
    var window: UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "SignUpSegue") {
            let signUpView = segue.destinationViewController as SignUpController
            signUpView.delegate = self
            //let child = segue.destinationViewController as ChildVC
            //child.text = textfield.text
            //child.delegate = self
        }
        else if(segue.identifier == "LogInSegue") {
            let mapView = segue.destinationViewController as MapController
            //mapView.delegate = self
            window = UIWindow(frame: UIScreen.mainScreen().bounds)
            
            let containerViewController = ContainerViewController()
            
            window!.rootViewController = containerViewController
            window!.makeKeyAndVisible()
        }
        
        
    }
    
    
    func done(child: SignUpController) {
        NSLog("Happen!")
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
