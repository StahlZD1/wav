//
//  ProfileController.swift
//  wav
//
//  Created by David Goehring on 2/18/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import UIKit

protocol ProfileDelegate {
    func done(child: ProfileController)
}

class ProfileController: UIViewController {

    
    var delegate: ProfileDelegate!
    
    @IBAction func backPress() {
        delegate.done(self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
