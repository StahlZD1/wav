//
//  ParseStuff.swift
//  wav
//
//  Created by James McKay on 2/12/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import Foundation

protocol nearbyAlertsHandler {
    func doneLoading()
    func addToArray(object : AnyObject)
}

func returnParseObjects(delegate : nearbyAlertsHandler) {
    
    
    var Q = PFQuery(className: "wavAlert")
    Q.whereKey("location", nearGeoPoint: PFGeoPoint(latitude: locationManager.location.coordinate.latitude, longitude: locationManager.location.coordinate.longitude), withinMiles: 1)
    Q.findObjectsInBackgroundWithBlock {
        (objects: [AnyObject]!, error: NSError!) -> Void in
        if error == nil {
            // The find succeeded.
            // Do something with the found objects
            for object in objects {
                delegate.addToArray(object)
            }
        } else {
            // Log details of the failure
            //NSLog("Error: %@ %@", error, error.userInfo!)
        }
        delegate.doneLoading()
    }
    
}