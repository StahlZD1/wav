//
//  MapController.swift
//  wav
//
//  Created by David Goehring on 2/18/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import UIKit
import MapKit

@objc
protocol CenterViewControllerDelegate {
    optional func toggleLeftPanel()
    optional func collapseSidePanels()
}

class MapController: UIViewController, ProfileDelegate, MKMapViewDelegate, SidePanelViewControllerDelegate, LayerToggleDelegate, nearbyAlertsHandler {
    
    //@IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var mapView : MKMapView!
    @IBOutlet weak var locationButton: UIButton!
    var delegate: CenterViewControllerDelegate?
    
    
    
    var panelViewController: sidePanelViewController!
    
    var nearbyAlerts : [AnyObject]!
    
    func doneLoading() {
        //NSLog("Done Loading \(nearbyAlerts.count) Items")
        for object in nearbyAlerts {
            var temp = PFObject(className: "wavAlert")
            temp = object as PFObject
            let temp2 = temp["location"]
            
            var pin = MKPointAnnotation()
            pin.coordinate = CLLocationCoordinate2D(latitude: temp2.latitude, longitude: temp2.longitude)
            pin.title = temp["description"] as String
            mapView.addAnnotation(pin)

        }
    }
    
    func addToArray(object: AnyObject) {
        nearbyAlerts.append(object)
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //locationManager.delegate = self
        //locationManager.requestAlwaysAuthorization()
        
        mapView.delegate = self
        var currentLocation = locationManager.location
        
        
        let loc = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        let span = MKCoordinateSpanMake(1 , 1)
        let reg = MKCoordinateRegionMake(loc, span)
        mapView.setRegion(reg, animated: false)
        
        // display user's location with a blue dot
        mapView.showsUserLocation = true;
        
        
    }
    
    override func awakeFromNib() {
        nearbyAlerts = []
        returnParseObjects(self)
        super.awakeFromNib()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "ProfileSegue") {
            let profileView = segue.destinationViewController as ProfileController
            profileView.delegate = self
        }
    }
    func done(child: ProfileController) {
        NSLog("Returned from Profile View")
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func toggleLayer(layer: String, state: Bool) {
        switch layer {
            case "Points of Interest":
                //mapView.mapType = kGMSTypeNormal
                break
            case "NOAA Data":
                //mapView.mapType = kGMSTypeSatellite
                break
            case "ARGUS Data":
                //mapView.mapType = kGMSTypeHybrid
                break
            case "Satellite":
                if state {
                    mapView.mapType = MKMapType.Satellite
                    //mapView.mapType = kGMSTypeSatellite
                }
                else {
                    mapView.mapType = MKMapType.Standard
                    //mapView.mapType = kGMSTypeNormal
                }
            case "Rain":
                //mapView.mapType = kGMSTypeNormal
                break
            case "Temperature":
                //mapView.mapType = kGMSTypeNormal
                break
            case "Wind":
                //mapView.mapType = kGMSTypeNormal
                break
            case "Pressure":
                //mapView.mapType = kGMSTypeNormal
                break
            default:
                mapView.mapType = mapView.mapType
                break
        }

    }
    
    @IBAction func centerOnUsersLocation(sender:UIButton){
        var currentLocation = locationManager.location
        
        
        let loc = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        let span = MKCoordinateSpanMake(1 , 1)
        let reg = MKCoordinateRegionMake(loc, span)
        mapView.setRegion(reg, animated: false)
    }
    
    @IBAction func slideOutTapped(sender: AnyObject) {
        delegate?.toggleLeftPanel?()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func layerSelected(layer: String) {
        switch layer {
        case "Points of Interest":
            //mapView.mapType = kGMSTypeNormal
            mapView.mapType = MKMapType.Standard
        case "NOAA Data":
            //mapView.mapType = kGMSTypeSatellite
            break
        case "ARGUS Data":
            //mapView.mapType = kGMSTypeHybrid
            break
        case "Satellite":
            mapView.mapType = MKMapType.Standard
            //mapView.mapType = kGMSTypeSatellite
        case "Rain":
            //mapView.mapType = kGMSTypeNormal
            break
        case "Temperature":
            //mapView.mapType = kGMSTypeNormal
            break
        case "Wind":
            //mapView.mapType = kGMSTypeNormal
            break
        case "Pressure":
            //mapView.mapType = kGMSTypeNormal
            break
        default:
            mapView.mapType = mapView.mapType
        }
        delegate?.toggleLeftPanel?()
    }
    
    // 1
    /*func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        // 2
        if status == .AuthorizedWhenInUse {
            
            // 3
            locationManager.startUpdatingLocation()
            
            //4
            //mapView.myLocationEnabled = true
            //mapView.settings.myLocationButton = true
        }
    }
    
    // 5
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        if let location = locations.first as? CLLocation {
            
            // 6
            //mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            
            // 7
            locationManager.stopUpdatingLocation()
        }
    }*/

}
